import React from "react";
import { Routes, Route } from "react-router-dom";
// styles
import "./App.css";
//components
const Navbar = React.lazy(() => import("./components/Navbar/Navbar"));
const Home = React.lazy(() => import("./components/Home/Home"));
const Footer = React.lazy(() => import("./components/Footer/Footer"));
const ServicesPage = React.lazy(() =>
  import("./components/ServicesPage/ServicesPage")
);
const AboutMe = React.lazy(() => import("./components/AboutMe/AboutMe"));
const PortfolioPage = React.lazy(() =>
  import("./components/PortfolioPage/PortfolioPage")
);
const PortfolioOne = React.lazy(() =>
  import("./components/AllPortfolio/Portfolio-one/Portfolio-one")
);
const Application = React.lazy(() =>
  import("./components/Application/Application")
);
const PortfolioTwo = React.lazy(() =>
  import("./components/AllPortfolio/Portfolio-two/Portfolio-two")
);
const PortfolioThree = React.lazy(() =>
  import("./components/AllPortfolio/Portfolio-three/Portfolio-three")
);

// import Navbar from "./components/Navbar/Navbar";
// import Home from "./components/Home/Home";
// import Footer from "./components/Footer/Footer";
// import ServicesPage from "./components/ServicesPage/ServicesPage";
// import AboutMe from "./components/AboutMe/AboutMe";
// import PortfolioPage from "./components/PortfolioPage/PortfolioPage";
// import PortfolioOne from "./components/AllPortfolio/Portfolio-one/Portfolio-one";
// import Application from "./components/Application/Application";
// import PortfolioTwo from "./components/AllPortfolio/Portfolio-two/Portfolio-two";
// import PortfolioThree from "./components/AllPortfolio/Portfolio-three/Portfolio-three";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route
          path="/"
          element={
            <React.Suspense>
              <Home />
            </React.Suspense>
          }
        />
        <Route
          path="/service"
          element={
            <React.Suspense>
              <ServicesPage />
            </React.Suspense>
          }
        />
        <Route
          path="/about-me"
          element={
            <React.Suspense>
              <AboutMe />
            </React.Suspense>
          }
        />
        <Route
          path="/portfolio"
          element={
            <React.Suspense>
              <PortfolioPage />
            </React.Suspense>
          }
        />
        <Route
          path="/portfolio-one"
          element={
            <React.Suspense>
              <PortfolioOne />
            </React.Suspense>
          }
        />
        <Route
          path="/portfolio-two"
          element={
            <React.Suspense>
              <PortfolioTwo />
            </React.Suspense>
          }
        />
        <Route
          path="/portfolio-three"
          element={
            <React.Suspense>
              <PortfolioThree />
            </React.Suspense>
          }
        />
        <Route
          path="/application"
          element={
            <React.Suspense>
              <Application />
            </React.Suspense>
          }
        />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
