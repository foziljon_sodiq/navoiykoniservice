import React from "react";
import "./Comment.scss";

const Comment = () => {
  return (
    <>
      <section className="comment-section">
        <div className="container">
          <div className="row">
            <div className="col">
              <h1>Отзывы о нашей компании</h1>
              <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
              >
                <div className="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    aria-label="Slide 1"
                    className="active"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                  {/* <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="3"
                    className="active"
                    aria-current="true"
                    aria-label="Slide 4"
                  ></button> */}
                </div>
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <p>
                      Был приятно удивлен хорошей работой коллектива ООО "Navoiy
                      Koni Servis". На всех этапах, от согласования проекта до
                      окончания строительства, не возникло никаких проблем
                      (сроки, комплектация поставки, качество работы). Все
                      возникающий вопросы оперативно решались с руководителем
                      компании Каюмов С. Спасибо!
                    </p>
                    <div>
                      <h3>Лей Хонгшен</h3>
                      <p>Менеджер проекта UNF</p>
                    </div>
                  </div>
                  <div className="carousel-item">
                    {" "}
                    <p>
                      Добрый день! Сотрудничали с компанией "Navoiy Koni
                      Servis". Впечатления остались положительные. Химический
                      проект реализовали довольно быстро, качественно и в срок!
                      Спасибо Вам большое!
                    </p>
                    <h3>Мистер Авшин</h3>
                    <p>Главный инженер</p>
                  </div>
                  <div className="carousel-item">
                    {" "}
                    <p>
                      Выражаю огромную признательность и благодарность
                      коллективу компании ООО «Navoiy Koni Servis» за
                      организацию поставок, на объект строительства: г. Навоий,
                      ПУ «Sinocemtech International Engineering Co.,LTD».
                      Актуальное наличие на складе, отсутствие сбоев в
                      поставках, доброжелательное отношение при самовывозе,
                      четкая и сбалансированная работа на каждом этапе сделки –
                      это показатели настоящих профессионалов своего дела! За
                      время совместной работы компания ООО «Navoiy Koni Servis»
                      зарекомендовала себя как честный и надежный поставщик.
                    </p>
                    <h3>Яо Ёнгхуа</h3>
                    <p>Заместитель менеджер проекта</p>
                  </div>
                  {/* <div className="carousel-item">
                    {" "}
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aspernatur, neque? Natus quo tempora laborum repellat
                      quidem aperiam culpa sunt excepturi eveniet sequi, nemo
                      qui voluptatem aliquid, libero esse dolorem labore.
                    </p>
                    <h3>John Doe 4</h3>
                    <p>CEO Compony Name</p>
                  </div> */}
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Comment;
