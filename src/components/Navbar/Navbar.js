import React from "react";
import "./Navbar.scss";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <section className="navbar-section sticky">
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light ">
          <div className="container-fluid navbar-brand">
            <Link to="/" className="brand-name">
              NAVOIY KONI SERVIS
            </Link>
            <span
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="navbarSupportedContent"
            >
              <span className="navbar-toggler-icon"></span>
            </span>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <select defaultValue={"UZ"}>
                    <option value={"UZ"}>UZ</option>
                    <option value={"ENG"}>ENG</option>
                    <option value={"RU"}>RU</option>
                  </select>
                </li>
                <li className="nav-item">
                  <i className="fa-regular fa-envelope"></i>terry.co.ltd@mail.ru
                </li>
                <li className="nav-item">
                  <i className="fa-solid fa-phone"></i>(+998) 90 732 99 99
                </li>
                <li className="nav-item">
                  <Link to="/">Главная</Link>
                </li>
                <li className="nav-item">
                  <Link to="service">Услуги</Link>
                </li>
                <li className="nav-item">
                  <Link to="portfolio">Проекты</Link>
                </li>
                <li className="nav-item">
                  <Link to="about-me">О Нас</Link>
                </li>
              </ul>
              <form className="d-flex">
                <Link
                  className="bg-danger application"
                  // type="submit"
                  to="application"
                >
                  Оставить Заявку
                </Link>
              </form>
            </div>
          </div>
        </nav>
      </div>
    </section>
  );
};

export default Navbar;
