import React from "react";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

//
import rasm from "../../image/plx-dark-default.webp";
import o_nas from "../../image/o-nas.webp";
import industry from "../../image/photo-sm-main-g.webp";
//styles
import "./About.scss";
const About = () => {
  return (
    <>
      <section className="about-section">
        <div className="container">
          <div className="row desktop">
            <h2> О компании</h2>
            <div className="col-7-lg col-md-7 col-sm-12 about-text">
              <h3>Мы строительная компания с 13-летним опытом</h3>

              <p>
                ООО <b>«НАВОИ КОНИ СЕРВИС»</b> создано в 2009 году для
                выполнения строительных работ на промышленных объектах с высоким
                уровнем потенциальной опасности. Течении тринадцать лет компания
                подписала более 635 контрактов. Сегодня в компании работает 85
                инженеров и техников и более 240 сотрудников. Главной целью
                компании является предоставление безопасных, быстрых и
                качественных услуг.
              </p>
              <p className="read-more">
                <Link to="about-me">Подробнеe...</Link>
              </p>
            </div>
            <div className="col-5-lg col-md-5 col-sm-12 about-img">
              <LazyLoadImage effect="blur" src={o_nas} alt="img" />
            </div>
          </div>
          <div className="row mobile">
            <h2>О Нас</h2>
            <div className="col-5-lg col-md-5 col-sm-12 about-img">
              <LazyLoadImage effect="blur" src={industry} alt="img" />
            </div>
            <div className="col-7-lg col-md-7 col-sm-12 about-text">
              <h3>Мы строительная компания с 13-летним опытом</h3>

              <p>
                ООО «НАВОИ КОНИ СЕРВИС» создано в 2009 году для выполнения
                строительных работ на промышленных объектах с высоким уровнем
                потенциальной опасности. Течении тринадцать лет компания
                подписала более 635 контрактов. Сегодня в компании работает 85
                инженеров и техников и более 240 сотрудников. Главной целью
                компании является предоставление безопасных, быстрых и
                качественных услуг.
              </p>
              <p className="read-more">
                <Link to="about-me">Подробнеe...</Link>
              </p>
            </div>
          </div>
        </div>
        <div
          className="col back-img"
          style={{ backgroundImage: `url(${rasm})` }}
        >
          <div className="overlay">
            <div className="container">
              <h2>Почему нам стоит доверять</h2>
              <div className="overlay-text">
                <h3>Сервис</h3>
                <p>
                  Мы обеспечиваем индивидуальный подход к каждому клиенту с
                  учетом его возможностей и пожеланий при постройке объекта.
                </p>
              </div>
              <div className="overlay-text">
                <h3>Прозрачность</h3>
                <p>
                  Мы ничего не скрываем от своих заказчиков. Наши клиенты могут
                  наблюдать за ходом строительства в режиме 24/7.
                </p>
              </div>
              <div className="overlay-text">
                <h3>Качество</h3>
                <p>
                  Наша цель не объем строительства, а его качество. Все этапы
                  строительства проходят стандартизацию и соответствуют
                  требованиям Градостроительного кодекса РУз.
                </p>
              </div>
              <div className="overlay-text">
                <h3>Доверие</h3>
                <p>
                  Наша компания зарекомендовала себя на строительном рынке как
                  один из надежных строителей. Мы помогаем решить самые разные
                  вопросы и постоянно взаимодействуем с заказчиками.
                </p>
              </div>
              <div className="overlay-text">
                <h3>Инновации</h3>
                <p>
                  При реализации мы используем только лучшие решения и передовые
                  технологии, будь то сырье для строительства или архитектура
                  здания.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default About;
