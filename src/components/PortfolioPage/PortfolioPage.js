import React from "react";
import "./PortfolioPage.scss";

import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

import { Link } from "react-router-dom";
//images
import main_1 from "../../image/Main-1.webp";
import p_img_main_1 from "../../image/portfoli-1-main.webp";
import p_img_main_2 from "../../image/portfolio-2-1.webp";
import p_img_main_3 from "../../image/portfolio-3-main.webp";

const PortfolioPage = () => {
  return (
    <>
      <section className="portfolio-page-section">
        <div className="section-portfolio-page-img">
          <LazyLoadImage effect="blur" src={main_1} alt="img" />
        </div>
        <div className="container">
          <div className="row">
            <h1>Наши проекты</h1>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <div className="portfolio-images">
                  <LazyLoadImage
                    effect="blur"
                    src={p_img_main_1}
                    alt="images"
                  />
                  <span className="overlay">
                    <span className="read-more">
                      <Link to="/portfolio-one">Все Фото</Link>
                    </span>
                  </span>
                </div>
                <h3>Строительные работы в проекте UNF</h3>
                <p className="read-more">
                  <Link to="/portfolio-one">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <div className="portfolio-images">
                  <LazyLoadImage
                    effect="blur"
                    src={p_img_main_2}
                    alt="images"
                  />
                  <span className="overlay">
                    <span className="read-more">
                      <Link to="/portfolio-two">Все Фото</Link>
                    </span>
                  </span>
                </div>
                <h3>Строительные работы в Кизилкумцементе </h3>
                <p className="read-more">
                  <Link to="/portfolio-two">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <div className="portfolio-images">
                  <LazyLoadImage
                    effect="blur"
                    src={p_img_main_3}
                    alt="images"
                  />
                  <span className="overlay">
                    <span className="read-more">
                      <Link to="/portfolio-three">Все Фото</Link>
                    </span>
                  </span>
                </div>
                <h3>Строительные работы в xимическом заводе</h3>
                <p className="read-more">
                  <Link to="/portfolio-three">Подробнеe...</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PortfolioPage;
