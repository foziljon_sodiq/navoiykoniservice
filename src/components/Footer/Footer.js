import React from "react";
import "./Footer.scss";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <footer>
        <div className="container">
          <div className="row">
            <div className="col">
              <div>
                <ul className="footer-link">
                  <li className="footer-b">
                    <b>Links</b>
                  </li>
                  <li className="footer-links">
                    <Link to="/">Главная</Link>
                  </li>
                  <li className="footer-links">
                    <Link to="service">Услуги</Link>
                  </li>
                  <li className="footer-links">
                    <Link to="portfolio">Проекты</Link>
                  </li>
                  <li className="footer-links">
                    <Link to="about-me">О Нас</Link>
                  </li>
                </ul>
                <ul className="footer-contact">
                  <li className="footer-b">
                    <b>Контакты</b>
                  </li>
                  <li className="footer-links">
                    Адрес: <span>210600, г. Кармана ул. З.Муъиддинов д3</span>
                  </li>
                  <li className="footer-links">
                    Почта: <span> terry.co.ltd@mail.ru</span>
                  </li>
                  <li className="footer-links footer-phone-link">
                    <span className="footer-phone">Телефон:</span>{" "}
                    <span>
                      <p>(+998) 90 732 99 99</p>
                      <p>(+998) 99 750 5505</p>
                      <p>(+998) 91 333 3066</p>
                    </span>
                  </li>
                </ul>
                <ul className="footer-social-media">
                  <li className="footer-b">
                    <b className="footer-service">
                      <Link to="application">Оставить Заявку</Link>
                    </b>
                  </li>
                  <li className="footer-icon">
                    <i className="fa-brands fa-facebook-f"></i>
                    <i className="fa-brands fa-instagram"></i>
                    <i className="fa-brands fa-telegram"></i>
                    <i className="fa-brands fa-linkedin-in"></i>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
