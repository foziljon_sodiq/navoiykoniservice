import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

import "./Logo.scss";
import logo_1 from "../../image/logo-1.webp";
import logo_2 from "../../image/logo-2.webp";

const Logo = () => {
  return (
    <>
      <section className="logo-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-2 col-md-2 col-sm-2">
              <LazyLoadImage effect="blur" src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-2">
              <LazyLoadImage effect="blur" src={logo_2} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
              <LazyLoadImage effect="blur" src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
              <LazyLoadImage effect="blur" src={logo_2} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
              <LazyLoadImage effect="blur" src={logo_1} alt="" />
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4">
              <LazyLoadImage effect="blur" src={logo_2} alt="" />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Logo;
