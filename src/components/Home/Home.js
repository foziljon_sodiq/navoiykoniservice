import React from "react";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

// components
import About from "../About/About";
import Comment from "../Comment/Comment";
import Logo from "../Logo/Logo";
import Portfolio from "../Portfolio/Portfolio";
import Service from "../Service/Service";
// images
import main_1 from "../../image/Main-1.webp";
import main_2 from "../../image/Main-2.webp";
import main_3 from "../../image/Main-3.webp";
// styles
import "./Home.scss";

const Home = () => {
  return (
    <>
      <section className="home-section">
        <div
          id="carouselExampleCaptions"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-indicators d-none">
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div className="carousel-inner ">
            <div className="carousel-item active">
              <LazyLoadImage
                effect="blur"
                src={main_1}
                className="d-block w-100 fit-cover"
                alt="..."
                width={"100%"}
              />
              <div className="carousel-caption d-none d-md-block slide-text-1">
                <h5>НАША ЦЕЛЬ НЕ ОБЪЕМ СТРОИТЕЛЬСТВА, А ЕГО КАЧЕСТВА.</h5>
                <div className="carousel-button-div">
                  <span className="read-more">
                    <Link to="service">Подробнее</Link>
                  </span>
                  <Link to="application" className="about-me">
                    Связаться
                  </Link>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <LazyLoadImage
                effect="blur"
                src={main_2}
                className="d-block w-100 fit-cover"
                alt="..."
                width={"100%"}
              />
              <div className="carousel-caption d-none d-md-block slide-text-2">
                <h5>
                  {" "}
                  ЗДОРОВЫЕ И НАСТРОЕНИЕ ЧЕЛОВЕКА СЧИТАЮТСЯ ПЕРВООЧЕРЕДНЫМИ
                  ЗАДАЧАМИ.
                </h5>
                <div className="carousel-button-div">
                  <span className="read-more">
                    <Link to="service">Подробнее</Link>
                  </span>
                  <Link to="application" className="about-me">
                    Связаться
                  </Link>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <LazyLoadImage
                effect="blur"
                src={main_3}
                className="d-block w-100 fit-cover"
                alt="..."
                width={"100%"}
              />
              <div className="carousel-caption d-none d-md-block slide-text-3">
                <h5>ВЕДУЩИЙ ПОСТАВЩИК ПРОМЫШЛЕННЫХ РЕШЕНИЙ</h5>

                <div className="carousel-button-div">
                  <span className="read-more">
                    <Link to="service">Подробнее</Link>
                  </span>
                  <Link to="application" className="about-me">
                    Связаться
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </section>
      <Service />
      <About />
      <Portfolio />
      <Comment />
      <Logo />
    </>
  );
};

export default Home;
