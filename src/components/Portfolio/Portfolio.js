import React from "react";
import "./Portfolio.scss";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

import p_img_main_1 from "../../image/portfoli-1-main.webp";
import p_img_main_2 from "../../image/portfolio-2-1.webp";
import p_img_main_3 from "../../image/portfolio-3-main.webp";

const Portfolio = () => {
  return (
    <>
      <section className="portfolio-section">
        <div className="container">
          <h1>Наши проекты</h1>
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <LazyLoadImage effect="blur" src={p_img_main_1} alt="images" />
                <p className="portfolio-title">
                  Строительные работы в проекте UNF{" "}
                </p>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <LazyLoadImage effect="blur" src={p_img_main_2} alt="images" />
                <p className="portfolio-title">
                  Строительные работы в Кизилкумцементе{" "}
                </p>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 col-div">
              <div>
                <LazyLoadImage effect="blur" src={p_img_main_3} alt="images" />
                <p className="portfolio-title">
                  Строительные работы в xимическом заводе
                </p>
                <p className="read-more">
                  <Link to="portfolio">Подробнеe...</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Portfolio;
