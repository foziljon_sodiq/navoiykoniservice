import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

import "./Portfolio-one.scss";
import p_img_main_1 from "../../../image/portfoli-1-main.webp";
import portfolio_1_1 from "../../../image/portfoli-1-1.webp";
import portfolio_1_2 from "../../../image/portfoli-1-2.webp";
import portfolio_1_3 from "../../../image/portfoli-1-3.webp";
import portfolio_1_4 from "../../../image/portfoli-1-4.webp";
import portfolio_1_5 from "../../../image/portfoli-1-5.webp";
import portfolio_1_6 from "../../../image/portfoli-1-6.webp";
import portfolio_1_7 from "../../../image/portfoli-1-7.webp";
import portfolio_1_8 from "../../../image/portfoli-1-8.webp";
import portfolio_1_9 from "../../../image/portfoli-1-9.webp";
import portfolio_1_10 from "../../../image/portfoli-1-10.webp";
import portfolio_1_11 from "../../../image/portfoli-1-11.webp";
import portfolio_1_12 from "../../../image/portfoli-1-12.webp";
import portfolio_1_13 from "../../../image/portfoli-1-13.webp";
import portfolio_1_14 from "../../../image/portfoli-1-14.webp";
import portfolio_1_15 from "../../../image/portfoli-1-15.webp";
import portfolio_1_16 from "../../../image/portfoli-1-16.webp";
import portfolio_1_17 from "../../../image/portfoli-1-17.webp";
import portfolio_1_18 from "../../../image/portfoli-1-18.webp";
import portfolio_1_19 from "../../../image/portfoli-1-19.webp";
import portfolio_1_20 from "../../../image/portfoli-1-20.webp";
import portfolio_1_21 from "../../../image/portfoli-1-21.webp";
import portfolio_1_22 from "../../../image/portfoli-1-22.webp";
import portfolio_1_23 from "../../../image/portfoli-1-23.webp";
import portfolio_1_24 from "../../../image/portfoli-1-24.webp";
import portfolio_1_25 from "../../../image/portfoli-1-25.webp";
import portfolio_1_26 from "../../../image/portfoli-1-26.webp";
import portfolio_1_27 from "../../../image/portfoli-1-27.webp";
import portfolio_1_28 from "../../../image/portfoli-1-28.webp";
import portfolio_1_29 from "../../../image/portfolio-1-29.webp";
import portfolio_1_30 from "../../../image/portfolio-1-30.webp";
import portfolio_1_31 from "../../../image/portfolio-1-31.webp";
import portfolio_1_32 from "../../../image/portfolio-1-32.webp";
import portfolio_1_33 from "../../../image/portfolio-1-33.webp";

const PortfolioOne = () => {
  return (
    <>
      <section className="portfolio-one-section">
        <div className="section-portfolio-page-img">
          <LazyLoadImage effect="blur" src={p_img_main_1} alt="img" />
        </div>
        <div className="container">
          <div className="row">
            <h1>Наши проекты</h1>
            <div className="col-lg-8 col-md-8 col-sm-12">
              <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
              >
                {/* <div className="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="3"
                    className="active"
                    aria-current="true"
                    aria-label="Slide 4"
                  ></button>
                </div> */}
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <LazyLoadImage effect="blur" src={p_img_main_1} alt="img" />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_1}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_2}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_3}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_4}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_5}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_6}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_7}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_8}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_9}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_10}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_11}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_12}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_13}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_14}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_15}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_15}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_17}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_16}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_18}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_19}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_20}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_21}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_22}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_23}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_24}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_25}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_26}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_27}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_28}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_29}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_30}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_31}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_32}
                      alt="img"
                    />
                  </div>
                  <div className="carousel-item">
                    <LazyLoadImage
                      effect="blur"
                      src={portfolio_1_33}
                      alt="img"
                    />
                  </div>
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <h2>Строительные работы в проекте UNF</h2>
              <ul>
                <li>
                  <b>Год:</b> 2019 - 2021
                </li>
                <li>
                  <b>Место:</b> город Навои
                </li>
                <li>
                  <b>Заказчик:</b> China National Chemical Engineering №7
                  Construction Co, LTD
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PortfolioOne;
