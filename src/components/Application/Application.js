import React, { useState } from "react";
import "./Application.scss";
import axios from "axios";

const Application = () => {
  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const onChangeEmail = (e) => {
    e.preventDefault();
    setEmail(e.target.value);
  };
  const onChangeUserName = (e) => {
    e.preventDefault();
    setUserName(e.target.value);
  };
  const onChangePhoneNumber = (e) => {
    e.preventDefault();
    setPhoneNumber(e.target.value);
  };

  const sendBtn = () => {
    let obj = {
      email: email,
      fullName: userName,
      phoneNumber: phoneNumber,
    };
    axios
      .post(`https://westco1.herokuapp.com/user/sign-up`, obj)
      .then((res) => {
        console.log(res);
      });

    // const timer = setTimeout(() => {
    //   window.location.reload(false)
    // }, 3000);
    // return () => clearTimeout(timer);
  };

  return (
    <>
      <section className="application-section">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="contact-application">
                <div>
                  <p>Ваше имя</p>
                  <input
                    type="text"
                    placeholder="Азиз"
                    onChange={onChangeUserName}
                  />
                  <p>
                    Телефон <span style={{ color: "red" }}>*</span>
                  </p>
                  <input
                    type="number"
                    placeholder="907329999 "
                    onChange={onChangePhoneNumber}
                  />
                  <p>Электронный адрес</p>
                  <input
                    type="email"
                    placeholder=" navoikoniservis@gmail.com"
                    onChange={onChangeEmail}
                  />
                  <button onClick={sendBtn}>отправить</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Application;
